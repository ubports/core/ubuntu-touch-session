# So QtQuick Controls 2 apps default to Suru style
export QT_QUICK_CONTROLS_STYLE=Suru

# For Kirigami
export QT_QUICK_CONTROLS_MOBILE=1

#
# Qt Auto-Scaling needs more testing. Disabling it for now.
# For current status, see:
# https://gitlab.com/groups/ubports/development/core/-/epics/39
#
# Enable automatic scaling based on screen pixel density
#export QT_AUTO_SCREEN_SCALE_FACTOR=1

# Silence 'Connections' and 'Binding.restoreMode' warnings.
# https://gitlab.com/ubports/development/core/ubuntu-touch-session/-/issues/29
export QT_LOGGING_RULES="qt.qml.connections.warning=false;qt.qml.binding.restoreMode.warning=false"
