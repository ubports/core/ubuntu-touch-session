# Export environment variables suitable for regular Ubuntu Touch environments,
# specifically Wayland on libhybris-based devices. For use on SDL and GTK apps.

export SDL_VIDEODRIVER=wayland
export GDK_DEBUG=gl-gles
export GDK_GL=gles
